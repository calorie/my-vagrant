my-vagrant
==========

## Requirements

- virtualbox
- git
- ruby
- chef
- knife-solo
- vagrant
- bershelf

## Setup

```
$ berks install -p chef/cookbooks
$ vagrant up
```

[ useradd ]( https://github.com/opscode-cookbooks/users#readme )

```
$ mkdir chef/data_bags/users
$ vim chef/data_bags/users/ur_user_id.json
```

