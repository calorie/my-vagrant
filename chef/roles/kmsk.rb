name "kmsk"
description "kmsk env"
run_list(
  "recipe[mysql]",
  "recipe[memcached]",
  "recipe[cassandra]",
  "recipe[redis]"
)
