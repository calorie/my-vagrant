name "dev"
description "dev env"
run_list(
  "recipe[desktop]",
  "recipe[curl]",
  "recipe[git]",
  "recipe[zsh]",
  "recipe[vim]",
  "recipe[ruby_build]",
  "recipe[rbenv::system]",
  "recipe[ctags]",
  "recipe[dotfiles]"
)
