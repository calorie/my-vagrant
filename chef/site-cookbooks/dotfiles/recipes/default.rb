# data_bag('users') do |u|
search("users", "groups:sysadmin NOT action:remove") do |u|

  user_name  = u['id']
  user_group = user_name
  user_home  = "/home/#{user_name}"

  dotdir = "#{user_home}/dotfiles"
  git dotdir do
    repository 'https://bitbucket.org/calorie/dotfiles-light.git'
    user       user_name
    group      user_group
    reference  'master'
    action     :checkout

    not_if { File.exists?(dotdir) }
  end

  execute "exec neobundle" do
    command "su #{user_name} -l -c 'echo y|#{dotdir}/setup.sh'"
  end
end
