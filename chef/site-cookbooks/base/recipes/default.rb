#
# Cookbook Name:: base
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
execute "update package index" do
  command "apt-get update"
  ignore_failure true
  action :nothing
end.run_action(:run)

include_recipe 'apt'
include_recipe 'users::sysadmins'
node.override['authorization']['sudo']['passwordless'] = true
include_recipe 'sudo'

package 'language-pack-ja' do
  action :install
end
bash "japanese" do
  code <<-EOH
  sudo dpkg-reconfigure locales
  EOH
end

data_bag('users').each do |id|
  execute "set #{id} passwd to '#{id}'" do
    user id
    command "echo #{id}:#{id}|sudo chpasswd"
  end
end
