if platform?("debian", "ubuntu")
  package "exuberant-ctags" do
    action :install
  end
else
  package "ctags" do
    action :install
  end
end
